Brief: Footswitch is an interactive video installation which uses OSC commands to drive Resolume Arena composition whenever a camera detects a trigger.

Tech: openFrameworks 0.8, Xcode 5, Resolume Arena 4

How-to: Clone this into your ofRoot/apps/myApps. Open Arena composition from folder "arena". Fill up with your clips if necessary (layer 1 for backround, layers 2 to 5 for triggers). Attach the camera. Build and run the app.

User interface:
When program loads press 'c' to capture referent image.
Hold 1/2/3/4 and click four points to make the region of interest for 4 switches.
Attack roi-s to see triggers toggle.
