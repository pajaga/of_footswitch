//
//  Trigger.cpp
//  of_footswitch
//
//  Created by Pavle Petrovic on 11/25/13.
//
//

#include "Trigger.h"

/* Brief: Constructor, initializes flags
 * Info: 1st ROI is RED
 *       2nd ROI is GREEN
 *       3rd ROI is BLUE
 *       4th ROI is YELLOW
 */
Trigger::Trigger() {
  // Init the triggers: colors, flags, region of interrest map (roiMap)
  rois[0].color.setHex(0xff0000);
  rois[1].color.setHex(0x00ff00);
  rois[2].color.setHex(0x0000ff);
  rois[3].color.setHex(0xffff00);
    
  for (int i = 0; i < ROI_NUM; i++) {
    rois[i].set = false;
    rois[i].triggered = false;
  }
    
  pressCnt = 0;
  lastClick.on = 0;
}

/* Brief: Trigger init function
 * Info: Allocates memory for textures
 * Args: w - width
 *       h - height
 */
void Trigger::init(int w, int h) {
  width = w;
  height = h;
  mapTexture.allocate(width, height, GL_RGBA);
  roiMap.allocate(width, height, GL_RGBA);
}

/* Brief: Drawing of ROIs
 * Info: Draw ROIs
 * Args: (x, y) - upper left corner coordinates
 *        w     - width
 *        h     - height
 */
void Trigger::draw(int x, int y, int w, int h) {
  for (int t = 0; t < ROI_NUM; t++) {
    if (rois[t].set) {
      mapTexture.loadData(rois[t].roiMapPixels);
      mapTexture.draw(x,y,w,h); // alpha = 0
    }
  }
  if (lastClick.on) {
    ofSetColor(lastClick.color);
    ofCircle(lastClick.x, lastClick.y, 3);
  }
}

/* Brief: Positioning of verteces for ROIs
 * Info: Is called from mousePressed()
 * Args: key    - selects the ROI [0-4]
 *       (x, y) - sets the ROI corner
 */
int Trigger::setVertex(int key, int x, int y) {
  // If less than 4 verteces set, make another one
  if (pressCnt < ROI_VERT) {
    rois[key].set = false;
    rois[key].x[pressCnt] = lastClick.x = x;
    rois[key].y[pressCnt] = lastClick.y = y;
    pressCnt++;
    lastClick.color = rois[key].color;
    lastClick.on = true;
    return 0;
  } else {
    // If all verteces are set, make the ROI and color it
    rois[key].set = true;
    lastClick.on = false;
    // Draw the shape to Fbo
    roiMap.begin();
    ofBackground(0,0);
    ofSetColor(rois[key].color);
    ofSetPolyMode(OF_POLY_WINDING_ODD);
    ofBeginShape();
    for (int p = 0; p < ROI_VERT; p++)
      ofVertex(rois[key].x[p],rois[key].y[p]);
    ofEndShape();
    roiMap.end();
    roiMap.readToPixels(rois[key].roiMapPixels);
    //  Make a list of pixel coords within the ROI
    rois[key].roiLength = 0;
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        if ((int)rois[key].roiMapPixels.getColor(i,j).getBrightness() > 0) {
          rois[key].roiPixels[rois[key].roiLength].x = i;
          rois[key].roiPixels[rois[key].roiLength].y = j;
          rois[key].roiLength++;
        }
      }
    }
    return 1;
  }
}

void Trigger::clearState() {
  pressCnt = 0;
}

int Trigger::isSet(int x) {
  return rois[x].set;
}

void Trigger::setReference(ofPixels refPixels) {
  this->refPixels = refPixels;
}

/* Brief: Compares current map to referent ones
 * Args: key - selects the ROI [0-4]
 *       tmpPixels - current map
 */
void Trigger::cmpLevels(int key, ofPixels tmpPixels) {
  int i, diff, tmp, ref;
  // Loop through all the roi pixels and compare
  for (i = 0, diff = 0; i < rois[key].roiLength; i++) {
    tmp = (int)(tmpPixels.getColor(rois[key].roiPixels[i].x, rois[key].roiPixels[i].y)).getBrightness();
    ref = (int)(refPixels.getColor(rois[key].roiPixels[i].x, rois[key].roiPixels[i].y)).getBrightness();
    // If tmp and ref differ more than 10% (25 of 255) increase diff count
    diff += (abs(tmp - ref) > 25);
  }

  // Trigger if more than 15% of ROI is different
  if (diff > 0.15 * rois[key].roiLength) {
    rois[key].dbncCnt++;
    if (rois[key].triggered == 0 && rois[key].dbncCnt > 1) {
      rois[key].triggered = 1;
      printf("%d triggered\n", key+1);
      // CALLS RESOLUME HERE
      resolume.play(key+2);
    }
  } else {
    rois[key].dbncCnt = 0;
    if (rois[key].triggered == 1) {
      rois[key].triggered = 0;
      printf("%d released\n", key+1);
      resolume.stop(key+2);
    }
  }
}