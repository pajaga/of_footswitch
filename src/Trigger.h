//
//  Trigger.h
//  of_footswitch
//
//  Created by Pavle Petrovic on 11/25/13.
//
//

#pragma once

#include "ofMain.h"
#include "OscResolume.h"

#define ROI_NUM 4           // number of regions
#define ROI_VERT 4          // number of vertices per region
#define COLOR_DEPTH 256     // color depth
#define DEF_CAM_WIDTH 640
#define DEF_CAM_HEIGHT 480

struct pixel {
	int x, y;
};

struct roi {
	ofColor color;                                   // color of roi
	ofPixels roiMapPixels;                           // roi part of map
	int x[ROI_VERT], y[ROI_VERT];                    // vertices coords
	int set, triggered;                              // if roi is set
	pixel roiPixels[DEF_CAM_WIDTH * DEF_CAM_HEIGHT]; // roi pixel coords
	int roiLength;                                   // how many pixels in roi
    int dbncCnt;                                     // debouncing counter
};

struct click {
	ofColor color;
	int x, y;
	int on;
};

class Trigger {
public:
    Trigger();
    void		init(int width, int height);
    void		draw(int x, int y, int w, int h);
    int			setVertex(int key, int x, int y);
    void		clearState();
    void		cmpLevels(int key, ofPixels tmpPixels);
    void        setReference(ofPixels refPixels);
    int			isSet(int x);
    
    int			width;
    int			height;
    
    roi			rois[ROI_NUM];
    ofTexture	mapTexture;
    ofPixels    refPixels;
    ofFbo		roiMap;
    int			pressCnt;
    click		lastClick;
    
    OscResolume resolume;
};
