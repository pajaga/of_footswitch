#pragma once

#include <stdio.h>
#include "ofMain.h"
#include "Trigger.h"
#include "ofxOpenCv.h"

#define DECIMATE 6
#define GAUSS_BLUR 15

class testApp : public ofBaseApp{
    
public:
    
  void setup();
  void update();
  void draw();
    
  void keyPressed(int key);
  void keyReleased(int key);
  void mouseReleased(int x, int y, int button);

  ofVideoGrabber  vidGrabber;
  ofTexture       refFrame;
  ofTexture       tmpFrame;
  ofxCvColorImage tmpCvImage;
  ofxCvColorImage refCvImage;
  int             camWidth;
  int             camHeight;
    
  Trigger		  trigger;
  OscResolume     resolume;
  int			  keyActive;
  int			  frameCnt;
};
