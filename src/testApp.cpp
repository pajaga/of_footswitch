#include "testApp.h"

void testApp::setup(){
	// Size of the camera stream
	camWidth	= DEF_CAM_WIDTH;
	camHeight 	= DEF_CAM_HEIGHT;
    
	// Init the video grabber
	vidGrabber.setVerbose(true);
    // Choose the camera
	vidGrabber.setDeviceID(0);
	vidGrabber.initGrabber(camWidth,camHeight);
    
	// Init the trigger
	trigger.init(camWidth, camHeight);
    
	// Allocate frames
	refFrame.allocate(camWidth,camHeight, GL_RGB);
	tmpFrame.allocate(camWidth,camHeight, GL_RGB);
    refCvImage.allocate(camWidth, camHeight);
    tmpCvImage.allocate(camWidth, camHeight);
    
	keyActive = frameCnt = 0;
}

//--------------------------------------------------------------
void testApp::update(){
    trigger.resolume.listen();
    
    ofBackground(0,0,0);
    
	vidGrabber.update();
    
	// Decimate by 6
	if (vidGrabber.isFrameNew()) {
		if (frameCnt++ == DECIMATE) {
			ofPixels tmpPixels;
			tmpFrame.loadData(vidGrabber.getPixels(), camWidth, camHeight, GL_RGB);
			tmpFrame.readToPixels(tmpPixels);
            // Blur image to make comparison more robust
            tmpCvImage.setFromPixels(tmpPixels);
            tmpCvImage.blurGaussian(GAUSS_BLUR);
            tmpPixels.setFromPixels(tmpCvImage.getPixels(), camWidth, camHeight, 3);
			frameCnt = 0;
			for (int t = 0; t < ROI_NUM; t++) {
                if (trigger.isSet(t)) {
                    trigger.cmpLevels(t, tmpPixels);
                }
			}
		}
	}
}

//--------------------------------------------------------------
void testApp::draw(){
	ofSetHexColor(0xffffff);
	// Draw current frame
	vidGrabber.draw(0, 0, camWidth, camHeight);
	// Draw referent frame
    refFrame.draw(camWidth, 0, 320, 240);
    
	// Draw the ROI maps
	ofEnableAlphaBlending();
	trigger.draw(0, 0, camWidth, camHeight);
	trigger.draw(camWidth, 0, 320, 240);
    
	// Draw the triggers
	for (int i = 0; i < ROI_NUM; i++) {
		if (trigger.rois[i].triggered) {
			ofSetColor(trigger.rois[i].color);
			ofRect(camWidth + 20 + i*25, 300, 20, 20);
		}
	}
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
    // Set the active key for ROI selection
	if (!keyActive && (key >= '1' && key <= '4'))
		keyActive = key;
    
    // Capture the referent frame
	if (key == 'c' || key == 'C') {
		ofPixels refPixels;
		// This is left from the original example, probably should be left out
		refFrame.loadData(vidGrabber.getPixels(), camWidth, camHeight, GL_RGB);
		refFrame.readToPixels(refPixels);
        // Blur image to make comparison more robust
        refCvImage.setFromPixels(refPixels);
        refCvImage.blurGaussian(GAUSS_BLUR);
        refPixels.setFromPixels(refCvImage.getPixels(), camWidth, camHeight, 3);
        refFrame.loadData(refCvImage.getPixels(), camWidth, camHeight, GL_RGB);
        trigger.setReference(refPixels);
	}
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){
	keyActive = 0;
	trigger.clearState();
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
	ofPixels tmpPixels;
	tmpFrame.loadData(vidGrabber.getPixels(), camWidth, camHeight, GL_RGB);
	tmpFrame.readToPixels(tmpPixels);
    
	// Set verteces for ROIs
	if (keyActive) {
		if (trigger.setVertex(keyActive - '1', x, y)) {
			printf("Object %c done!\n", keyActive);
		}
	}
}
