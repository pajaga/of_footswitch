//
//  OscResolume.h
//  of_footswitch
//
//  Created by Aleksandar Abu-Samra on 11/25/13.
//
//
#pragma once

#include "ofxOsc.h"

#define HOST "127.0.0.1"
#define OSC_OUT_PORT 7000
#define OSC_IN_PORT 7001

#define NUM_LAYERS 5
#define NUM_OF_SCENES 1 // Num of scenes/columns in resolume
#define SCENE_TIME_IN_MINUTES 60

class OscResolume {
private:
	static const bool backgroundAlwaysOn = false;
	static int scene; // scene id (column number)

	bool active[NUM_LAYERS]; // active layers array

	int charToInt(char);
	char intToChar(int);

	void playBackgroundIfNeeded();

	bool sceneSet;
	void trackTime();

public:
	ofxOscSender sender;
	ofxOscReceiver receiver;

	OscResolume(void);
	~OscResolume(void);

	void send(string address, int arg);
	void send(string address, float arg);

	void reset();
	void play(int layerID, int clipID = scene);
	void stop(int layerID);
	void listen();
	void nextScene();
};
