#include "OscResolume.h"

int OscResolume::scene = 1; // active scene

OscResolume::OscResolume(void) {
	sender.setup(HOST, OSC_OUT_PORT);
	receiver.setup(OSC_IN_PORT);

	sceneSet = false;
	reset();
}

OscResolume::~OscResolume(void) {}

int OscResolume::charToInt(char c) { return c - 48; }
char OscResolume::intToChar(int i) { return i + 48; }

// Sends an INT argument OSC command
void OscResolume::send(string address, int arg) {
	ofxOscMessage m;
	m.setAddress(address);
	m.addIntArg(arg);
	sender.sendMessage(m);
}

// Sends a FLOAT argument OSC command
void OscResolume::send(string address, float arg) {
	ofxOscMessage m;
	m.setAddress(address);
	m.addFloatArg(arg);
	sender.sendMessage(m);
}

// Resets Resolume
void OscResolume::reset() {

	// Fade in
	string transitiontime = "/layerX/transitiontime";
	for (int i = 1; i <= NUM_LAYERS; i++) {
		transitiontime[6] = intToChar(i);
		send(transitiontime, (float)0.07);
	}

	// Play mode
	string playmode = "/layerX/clipY/video/position/playmode";
	playmode[12] = intToChar(scene); // set current scene

	// Turn on trigger
	playmode[6] = '6'; play(6);

	// Turn on screensaver in loop
	playmode[6] = '1'; send(playmode, 1); play(1);

	// Turn off everything else, but set loop
	playmode[6] = '2'; send(playmode, 1); stop(2);
	playmode[6] = '3'; send(playmode, 1); stop(3);
	playmode[6] = '4'; send(playmode, 1); stop(4);
	playmode[6] = '5'; send(playmode, 1); stop(5);
    
    // Set bpm to 130 to sync with audio
    send("/playbackcontroller/bpm/", (float)128.51/500);
}

void OscResolume::play(int layerID, int clipID) {
	// Command: /layerX/clipX/connect
	string address;
		address = "/layer";
		address += ofToString(layerID);
		address += "/clip";
		address +=  ofToString(clipID);
		address += "/connect";
	send(address, 1);
    
    string playmode = "/layerX/clipY/video/position/playmode";
	send(playmode, 1);

	if (!backgroundAlwaysOn && layerID > 1 && !active[layerID-1]) {
        stop(1);
        send("/playbackcontroller/resync/", 1);
    }
    
    active[layerID-1] = true;
}

void OscResolume::stop(int layerID) {
	// Command: /layerX/clear
	string address;
		address = "/layer";
		address += ofToString(layerID);
		address += "/clear";

	send(address, 1);
	active[layerID-1] = false;
}

// Plays background video (layer1) if no other layer is active
void OscResolume::playBackgroundIfNeeded() {
	bool playBackground = true;

	for (int i = 0; i < NUM_LAYERS; i++) {
		if (active[i]) {
			playBackground = false;
			break;
		}
	}

	if (playBackground) play(1);
}

void OscResolume::nextScene() {
	// Stop all
	stop(1);
	stop(2);
	stop(3);
	stop(4);
	stop(5);
	stop(6);

	// Next scene
	scene++;
	if (scene > NUM_OF_SCENES) scene = 1; // Reset if no more scenes

	reset();
}

// Listen to Resolume's OSC output
void OscResolume::listen() {

	trackTime();
	
	if (!backgroundAlwaysOn && !active[0]) {
        playBackgroundIfNeeded();
#if 0
    // This code checks for time elapsed in a clip
        // check for waiting messages
		while(receiver.hasWaitingMessages()){
			// get the next message
			ofxOscMessage m;
			receiver.getNextMessage(&m);
			
			string address = m.getAddress();
			string positionAddress = "/layerX/video/position/values";

			for (int i = 2; i <= 5; i++) {
				positionAddress[6] = intToChar(i);
				if (address == positionAddress) {
					float position = m.getArgAsFloat(0);
					if (position > 0.92) {
						active[i-1] = false;
					}
					break;
				}
			}
		}
#endif        
	}
}

void OscResolume::trackTime() {
	time_t seconds;
	seconds = time(NULL);

	int min = (seconds / 60) % SCENE_TIME_IN_MINUTES;
	if (min == 0 && !sceneSet) {
		sceneSet = true;
		nextScene();
	}
	else if (min == 1) {
		sceneSet = false;
	}
}